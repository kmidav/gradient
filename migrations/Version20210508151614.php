<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210508151614 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product ADD owner_id INT NOT NULL');
        $this->addSql('ALTER TABLE product ADD title VARCHAR(75) NOT NULL');
        $this->addSql('ALTER TABLE product ADD short_description VARCHAR(1024) DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD detail_description TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD price INT NOT NULL');
        $this->addSql('ALTER TABLE product ADD discount_price INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product RENAME COLUMN name TO slug');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD7E3C61F9 FOREIGN KEY (owner_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_D34A04AD7E3C61F9 ON product (owner_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE product DROP CONSTRAINT FK_D34A04AD7E3C61F9');
        $this->addSql('DROP INDEX IDX_D34A04AD7E3C61F9');
        $this->addSql('ALTER TABLE product DROP owner_id');
        $this->addSql('ALTER TABLE product DROP title');
        $this->addSql('ALTER TABLE product DROP short_description');
        $this->addSql('ALTER TABLE product DROP detail_description');
        $this->addSql('ALTER TABLE product DROP price');
        $this->addSql('ALTER TABLE product DROP discount_price');
        $this->addSql('ALTER TABLE product RENAME COLUMN slug TO name');
    }
}
