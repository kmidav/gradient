<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210508063620 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE "user" ADD first_name VARCHAR(50) DEFAULT NULL');
        $this->addSql('ALTER TABLE "user" ADD middle_name VARCHAR(50) DEFAULT NULL');
        $this->addSql('ALTER TABLE "user" ADD last_name VARCHAR(50) DEFAULT NULL');
        $this->addSql('ALTER TABLE "user" ADD mobile VARCHAR(15) DEFAULT NULL');
        $this->addSql('ALTER TABLE "user" ADD registered_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');
        $this->addSql('ALTER TABLE "user" ADD last_login TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE "user" DROP first_name');
        $this->addSql('ALTER TABLE "user" DROP middle_name');
        $this->addSql('ALTER TABLE "user" DROP last_name');
        $this->addSql('ALTER TABLE "user" DROP mobile');
        $this->addSql('ALTER TABLE "user" DROP registered_at');
        $this->addSql('ALTER TABLE "user" DROP last_login');
    }
}
