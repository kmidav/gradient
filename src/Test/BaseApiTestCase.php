<?php


namespace App\Test;


use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use \ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class BaseApiTestCase extends ApiTestCase
{
    protected function createUser(string $email, string $password): User
    {
        $user = new User();
        $user->setEmail($email);
        $encodedPassword = self::$container->get(UserPasswordEncoderInterface::class)
            ->encodePassword($user, $password);

        $user->setPassword($encodedPassword);

        $em = $this->getEntityManager();
        $em->persist($user);
        $em->flush();

        return $user;
    }

    protected function login(Client $client, string $email, string $password): Response
    {
        $response = $client->request('POST', '/api/login', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'email' => $email,
                'password' => $password
            ],
        ]);

        return $response;
    }

    protected function createUserAndLogin(Client $client,string $email, string $password): Response
    {
        $this->createUser($email, $password);
        $response = $this->login($client, $email, $password);

        return $response;
    }

    protected function getEntityManager(): EntityManagerInterface
    {
        return self::$container->get(EntityManagerInterface::class);
    }
}