<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CartRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\Timestampable;
use Symfony\Component\Serializer\Annotation\Groups;

// todo role owner voter
/**
 * @ApiResource(
 *     security="is_granted('ROLE_USER')",
 *     denormalizationContext={"groups"={"cart:write"}},
 *     normalizationContext={"groups"={"cart:read"}},
 *     itemOperations={
 *          "get",
 *          "put",
 *          "delete"
 *     },
 *     collectionOperations={
 *          "get",
 *          "post"
 *     },
 *     attributes={
 *          "pagination_items_per_page"=15
 *     }
 * )
 * @ORM\Entity(repositoryClass=CartRepository::class)
 */
class Cart
{
    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="cart", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"cart:read", "cart:write"})
     */
    private $customer;

    /**
     * @ORM\OneToMany(targetEntity=CartItem::class, mappedBy="cart",cascade={"persist", "remove"}, orphanRemoval=true)
     * @Groups({"cart:read", "cart:write"})
     */
    private $cartItems;

    public function __construct()
    {
        $this->cartItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCustomer(): ?User
    {
        return $this->customer;
    }

    public function setCustomer(User $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return Collection|CartItem[]
     */
    public function getCartItems(): Collection
    {
        return $this->cartItems;
    }

    public function addCartItem(CartItem $cartItem): self
    {
        foreach ($this->getItems() as $existingItem) {
            // The item already exists, update the quantity
            if ($existingItem->equals($cartItem)) {
                $existingItem->setQuantity(
                    $existingItem->getQuantity() + $cartItem->getQuantity()
                );
                return $this;
            }
        }

        $this->cartItems[] = $cartItem;
        $cartItem->setCart($this);

        return $this;
    }

    public function removeCartItem(CartItem $cartItem): self
    {
        if ($this->cartItems->removeElement($cartItem)) {
            // set the owning side to null (unless already changed)
            if ($cartItem->getCart() === $this) {
                $cartItem->setCart(null);
            }
        }

        return $this;
    }

    /**
     * Calculates the order total.
     *
     * @return float
     */
    public function getTotal(): float
    {
        $total = 0;

        foreach ($this->getOrderItems() as $item) {
            $total += $item->getTotal();
        }

        return $total;
    }
}
