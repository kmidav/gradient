<?php

namespace App\Entity;

use App\Repository\ProductMetaRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiResource;
/**
 * @ApiResource(
 *     security="is_granted('ROLE_ADMIN')",
 *     denormalizationContext={"groups"={"productMeta:write"}},
 *     normalizationContext={"groups"={"productMeta:read"}},
 *     itemOperations={
 *          "get"={"security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')"},
 *          "put",
 *          "delete"
 *     },
 *     collectionOperations={
 *          "get"={"security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')"},
 *          "post"
 *     },
 *     attributes={
 *          "pagination_items_per_page"=15
 *     }
 * )
 * @ORM\Entity(repositoryClass=ProductMetaRepository::class)
 */
class ProductMeta
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=Product::class, inversedBy="productMeta", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"productMeta:read", "productMeta:write"})
     */
    private $product;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"productMeta:read", "productMeta:write"})
     */
    private $content;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }
}
