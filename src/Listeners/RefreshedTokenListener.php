<?php


namespace App\Listeners;


use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;

// config services.yaml
class RefreshedTokenListener implements EventSubscriberInterface
{
    // change on https
    private $secure = false;
    private $ttl;

    public function __construct($ttl)
    {
        $this->ttl = $ttl;
    }

    public static function getSubscribedEvents()
    {
        return [
            'lexik_jwt_authentication.on_authentication_success' => [
                ['setRefreshToken']
            ]
        ];
    }

    public function setRefreshToken(AuthenticationSuccessEvent $event)
    {
        $refreshToken = $event->getData()['refresh_token'];
        $response = $event->getResponse();

        if ($refreshToken) {
            $response->headers->setCookie(
                new Cookie(
                    'REFRESH_TOKEN', $refreshToken,
                    (new \DateTime())
                        ->add(new \DateInterval('PT'. $this->ttl . 'S')),
                    '/',
                    null,
                    $this->secure
                )
            );
        }
    }
}