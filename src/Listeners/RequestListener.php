<?php


namespace App\Listeners;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class RequestListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [
                ['onKernelRequest']
            ]
        ];
    }

    /** trigger before controllers
        refresh token bundle не может прочитать токен из cookie
        поэтому перехватываем токен из cookie и передаем в body запроса
        чтобы bundle смог прочитать
    */
    public function onKernelRequest(RequestEvent $event)
    {
        $request = $event->getRequest();
        // bundle ищет refresh_token, можно сменить название в yml этого бандла
        $request->attributes->set('refresh_token', $request->cookies->get('REFRESH_TOKEN'));
    }
}