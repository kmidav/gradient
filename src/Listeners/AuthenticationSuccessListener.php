<?php


namespace App\Listeners;

# config in services.yaml
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\HttpFoundation\Cookie;

class AuthenticationSuccessListener
{
    // change on https
    private $secure = false;
    private $tokenTtl;

    // get token time to live from services.yaml
    public function __construct($tokenTtl)
    {
        $this->tokenTtl = $tokenTtl;
    }

    public function onAuthenticationSuccess(AuthenticationSuccessEvent $event)
    {
        // save token as a http only cookie not in the local js storage (unsafe)
        $response = $event->getResponse();
        $data = $event->getData();

        $token = $data['token'];

        $response->headers->setCookie(
          new Cookie(
              'BEARER', $token,
              (new \DateTime())
                    ->add(new \DateInterval('PT'. $this->tokenTtl . 'S')),
              '/',
              null,
              $this->secure
          )
        );
    }
}