# aliases for console commands
# in console just write:   make [command name]
up:
	docker-compose up -d

down:
	docker-compose down

rebuild:
	sudo rm -rf db-data
	docker-compose down -v --remove-orphans
	docker-compose rm -vsf
	docker-compose up -d --build

# delete and recreate database for example when migrations fails
# пропускать все вопросы флаг -n
db:
	docker-compose exec php ./bin/console doctrine:database:drop --force
	docker-compose exec php ./bin/console doctrine:database:create -n
	docker-compose exec php ./bin/console doctrine:migrations:migrate

migration:
	docker-compose exec php ./bin/console make:migration
migrate:
	docker-compose exec php ./bin/console doctrine:migrations:migrate

prod:
	docker-compose -f docker-compose_prod.yml up -d

prod_build:
	docker-compose -f docker-compose_prod.yml build

make test:
	docker-compose exec php bin/phpunit

# docker-compose exec php composer ...
# docker-compose exec php ./bin/console ...
