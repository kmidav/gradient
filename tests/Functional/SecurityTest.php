<?php


namespace App\Tests\Functional;


use App\Test\BaseApiTestCase;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;

class SecurityTest extends BaseApiTestCase
{
    use ReloadDatabaseTrait;

    public function testLogin(): void
    {
        $client = self::createClient();

        // unauthorized
        $this->createUser('user1@mail.ru', '12345');
        $client->request('GET', '/api/users');

        self::assertResponseStatusCodeSame(401);

        // login
        $response = $this->login($client, 'user1@mail.ru', '12345');
        $json = $response->toArray();

        self::assertResponseIsSuccessful();
        self::assertResponseHasCookie('BEARER');
        self::assertResponseHasCookie('REFRESH_TOKEN');
        self::assertArrayHasKey('token', $json);
        self::assertArrayHasKey('refresh_token', $json);
    }
}